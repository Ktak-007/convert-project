#!/usr/bin/python
# -*- coding: utf-8; tab-width: 4; -*-
# Скрипт для перекодировки проекта из KOI8-R в Windows-1251

from sys import *
from os import *
from os.path import *
from re import *
from shutil import *

koi = compile('KOI8-R').match
image = compile('(gif|png|jpg)$').search

def usage():
    print 'Правильный вызов: '+argv[0]+' <source dir> <destination dir>'
    exit(1)

def dir_iter(src, dst):
    """ Функция повторяет структуру каталога src в каталоге dst, перекодируя файлы """
    mkdir(dst)
    copymode(src, dst)
    for f in listdir(src):
        src_f = src+'/'+f
        dst_f = dst+'/'+f
        if isdir(src_f):
            dir_iter(src_f, dst_f)
        else:
            p = popen('enca '+src_f, 'r')
            if not image(f) and koi(p.readline()):
                system('echo '+src_f+'; iconv -f koi8-r -t cp1251 "'+src_f+'" > "'+dst_f+'"')
            else:
                copyfile(src_f, dst_f)
                copymode(src_f, dst_f)
            p.close()

def __main__():
    if len(argv) != 3:
        usage()

    src = argv[1]
    dst = argv[2]

    # Проверим, что исходный каталог существует
    if not isdir(src):
        print "Исходный каталог %s не найден!" % src
        usage()

    # Проверим, что каталог назначения не создан
    if exists(dst):
        print "Каталог назначения %s уже существует!" % dst
        usage()

    # Приведём пути каталогов к полным
    src = abspath(src)
    dst = abspath(dst)

    dir_iter(src, dst)

__main__()
